/**
 * @file
 * JavaScript file for the Coffee module.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  var PROCESSED_CLASS = 'jsonp-sparql-processed';
  var templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
  };

  var processData = function(settings, id, data) {
    var $el = settings.element.find('.wrapper');
    // See if the dataset is empty.
    if (!data || !data.results || !data.results.bindings) {
      // Treat as an error.
      return $el.html(Drupal.checkPlain(settings.error_message));
    }
    // See if the dataset is empty.
    if (!data.results.bindings.length) {
      return $el.html(Drupal.checkPlain(settings.empty_message));
    }
    // So, we have results. Trust that user to provide a good template, yeah?
    var items = data.results.bindings;
    var template = _.template(settings.data_template, templateSettings);
    var results = items.map(function(n) {
      var data = {};
      Object.keys(n).forEach(function(j) {
        data[j] = Drupal.checkPlain(n[j].value)
      });
      return template(data);
    });
    $el.html(results.join(''));
  };

  Drupal.behaviors.jsonpSparql = {
    attach: function(context) {
      $(context).find('[data-is-sparql-block]').each(function(i, n) {
        var $el = $(n);
        if ($el.hasClass(PROCESSED_CLASS)) {
          return;
        }
        // Mark as processed.
        $el.addClass(PROCESSED_CLASS);
        // Append element for ajax.
        $el.find('.wrapper')
          .append('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>');
        // Get the id.
        var id = $el.attr('data-id');
        // Find the settings for it.
        var settings = drupalSettings.jsonp_sparql[id];
        settings.element = $el;
        // Replace the query with the value.
        var query = _.template(settings.sparql_query, templateSettings)({
          value: settings.value
        });
        // Do an AJAX request, and try to massage it in some way.
        $.ajax({
          dataType: 'jsonp',
          type: 'GET',
          url: settings.endpoint,
          data: {
            query: query,
            format: 'json',
            output: 'json'
          },
          success: processData.bind(null, settings, id),
          error: function(e) {
            console.log(e)
          }
        });
      });
    },
    detach: function(context) {
    }
  }
})(jQuery, Drupal, drupalSettings);
