<?php

namespace Drupal\Tests\jsonp_sparql\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the most basic functionality of the module.
 *
 * @group jsonp_sparql
 */
class EnableTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'jsonp_sparql',
  ];

  /**
   * Test enable.
   */
  public function testEnable() {
    self::assertTrue(TRUE);
  }

}
